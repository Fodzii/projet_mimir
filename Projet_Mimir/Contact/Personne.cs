﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_Mimir
{
    public class Personne
    {
        private int id;
        private string image;
        private string civilite;
        private string nom;
        private string prenom;
        private string mail;
        private string telephone;
        private Type classe;


        /*Constructeur*/
        public Personne(int unId, string uneCivilite, string unNom, string unPrenom, string unMail=null, string unTelephone=null, string uneImage=null, Type uneClasse=null)
        {
            id = unId;
            image = uneImage;
            civilite = uneCivilite;
            nom = unNom;
            prenom = unPrenom;
            mail = unMail;
            telephone = unTelephone;
            classe = uneClasse;
        }

        /* Propriétés */
        public int Identifiant
        {
            get { return id; }
            set { id = value; }
        }

        public string Image
        {
            get { return image; }
            set { image = value; }
        }

        public string Civilite
        {
            get { return civilite; }
            set { civilite = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        public string Mail
        {
            get { return mail; }
            set { mail = value; }
        }

        public string NumeroTel
        {
            get { return telephone; }
            set { telephone = value; }
        }

        public string NomComplet
        {
            get { return Civilite + " " + Nom + " " + Prenom; }
        }

        public Type Classe
        {
            get { return classe; }
            set { classe = value; }
        }

        /* ToString */
        public override string ToString()
        {
            string numTel = null;
            string mail = null;

            if (null != NumeroTel) {
                numTel = "\nTéléphone: " + NumeroTel;
            }
            if (null != Mail)
            {
                mail = "\nMail: " + Mail;
            }

            return Nom + " " + Prenom + mail + numTel;
        }
    }
}
