﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Projet_Mimir
{
    public class PersonneViewModel
    {
        private ObservableCollection<Personne> personnes = new ObservableCollection<Personne>();
        public ObservableCollection<Personne> Personnes { get { return this.personnes; } }
        public PersonneViewModel()
        {
            this.personnes.Add(new Personne(1, "Mademoiselle", "Benarroche", "Marion", "zopinou06@hotmail.fr", null, "Assets/girl.png", typeof(Tic_tac)));
            this.personnes.Add(new Personne(2, "Monsieur", "Quachéro", "Alaric", "alaricquachero@gmail.com", "06 98 13 34 21", "Assets/boy.png", typeof(Tic_tac)));
            this.personnes.Add(new Personne(3, "Mademoiselle", "Rosso", "Jessica", "jessica.rosso@laposte.net", "06 29 76 89 68", "Assets/girl.png", typeof(Tic_tac)));
            this.personnes.Add(new Personne(4, "Monsieur", "Blancheton", "Fabien", "blancheton.fabien@gmail.com", "06 01 75 86 03", "Assets/boy.png", typeof(Tic_tac)));
        }
    }
}
