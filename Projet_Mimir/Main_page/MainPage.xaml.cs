﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Core;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Xaml.Media;

namespace Projet_Mimir
{

    public sealed partial class MainPage : Page
    {
        public Type lienEnigme;
        private Enigmes enigmes = new Enigmes();

        public MainPage()
        {
            this.InitializeComponent();
            SystemNavigationManager.GetForCurrentView().BackRequested += OnBackRequested;

            if (Application.Current.Resources.ContainsKey("enigmes"))
            {
               enigmes = (Enigmes)Application.Current.Resources["enigmes"];
            }
            else
            {
                Application.Current.Resources["enigmes"] = new Enigmes();
            }

            for(int i = 0; i<enigmes.Count; i++)
            {
                if (!enigmes[i].Debloquee)
                {
                    lienEnigme = enigmes[i - 1].Classe;
                    break;
                }
                else if(i == enigmes.Count - 1)
                {
                    lienEnigme = enigmes[i].Classe;
                }
            }
        }

        private void btnClicContacts_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Contact));
        }

        private void btnClicParametres_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnClicListeEnigmes_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ListeEnigmes));
        }

        private void btnClicJouer_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(lienEnigme);
        }


        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame.CanGoBack) { e.Handled = true; rootFrame.GoBack(); }
        }

        private void logo_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
             Image monImage = sender as Image;
             Storyboard sb = new Storyboard();
             Duration duration = new Duration(TimeSpan.FromSeconds(20));
             DoubleAnimation myDoubleAnimation1 = new DoubleAnimation();
             DoubleAnimation myDoubleAnimation2 = new DoubleAnimation();

             myDoubleAnimation1.EnableDependentAnimation = true;
             myDoubleAnimation1.Duration = duration;
             myDoubleAnimation2.EnableDependentAnimation = true;
             myDoubleAnimation2.Duration = duration;
            myDoubleAnimation2.From = 0;
            myDoubleAnimation2.EasingFunction = new PowerEase { EasingMode = EasingMode.EaseOut };

            sb.Duration = duration;
             sb.Children.Add(myDoubleAnimation1);
             sb.Children.Add(myDoubleAnimation2);
            Storyboard.SetTarget(myDoubleAnimation1, monImage);
             Storyboard.SetTargetProperty(myDoubleAnimation1, "Opacity");
            Storyboard.SetTarget(myDoubleAnimation2, monImage);
            Storyboard.SetTargetProperty(myDoubleAnimation2, "(monImage.RenderTransform).(RotateTransform.Angle)");
            // myDoubleAnimation1.From = myRectangle.Width;
            RotateTransform rt = new RotateTransform();
            monImage.RenderTransform = rt;
            myDoubleAnimation2.To = 360;
            myDoubleAnimation1.To = monImage.Opacity - 10;
            sb.Begin();


        }
    }

}

